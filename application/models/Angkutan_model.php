<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Angkutan_model extends CI_Model
{

    private $_table = 'm_angkutan';
    public function rules(){

        return   [
            ['field' => 'merek_mobil',
            'label' => 'Merek Mobil',
            'rules' => 'required'],

            ['field' => 'harga_mobil',
            'label' => 'Harga Mobil',
            'rules' => 'required'],

            ['field' => 'plat_nomor',
            'label' => 'Plat Nomor',
            'rules' => 'required'],

            ['field' => 'harga',
            'label' => 'Harga Sewa Perbulan',
            'rules' => 'required']

            
        ];

    }

    public function save()
    {
        $post = $this->input->post();
        $this->merek_mobil = $post["merek_mobil"];
        $this->harga_mobil = $post["harga_mobil"];
        $this->plat_nomor = $post["plat_nomor"];
        $this->harga = $post["harga"];
        $this->oleh = $this->session->userdata('user_logged')->user_id;
        return $this->db->insert($this->_table, $this);
    }

    public function getAll(){

        $sql = $this->db->select('a.id,b.mobil_nama,a.harga_mobil,a.harga')
                        ->from('m_angkutan a')
                        ->join('m_mobil b','b.mobil_id = a.merek_mobil');
        $query = $sql->get();

        return $query->result();

    }

    Public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }


    public function update()
    {
        $post = $this->input->post();
        $this->merek_mobil = $post["merek_mobil"];
        $this->harga_mobil = $post["harga_mobil"];
        $this->plat_nomor = $post["plat_nomor"];
        $this->harga = $post["harga"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }

}