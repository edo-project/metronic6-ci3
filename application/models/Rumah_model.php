<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rumah_model extends CI_Model
{
    private $_table = 'm_rumah';


    public function rules(){

        return   [
            ['field' => 'luas_tanah',
            'label' => 'Luas Tanah',
            'rules' => 'required'],

            ['field' => 'provinsi',
            'label' => 'Provinsi',
            'rules' => 'numeric'],
            
            ['field' => 'kotamadya',
            'label' => 'Kotamadya',
            'rules' => 'required'],

            ['field' => 'kecamatan',
            'label' => 'Kecamatan',
            'rules' => 'required'],

            ['field' => 'alamat',
            'label' => 'Alamat Rumah',
            'rules' => 'required'],

            ['field' => 'harga',
            'label' => 'Harga Sewa Perbulan',
            'rules' => 'required']

            
        ];

    }

    public function getAll()
    {
        // return $this->db->get($this->_table)->result();

        $sql = $this->db->select('a.id,a.luas_tanah,b.nama as nama_propinsi,a.harga,c.status')
        ->from('m_rumah a')
        ->join('m_ipropinsi b','b.id = a.provinsi')
        ->join('m_status c','c.id = a.status');
        $query = $sql->get();

        return $query->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->nama_penghuni = $post["nama_penghuni"];
        $this->luas_tanah = $post["luas_tanah"];
        $this->provinsi = $post["provinsi"];
        $this->kota = $post["kotamadya"];
        $this->kecamatan = $post["kecamatan"];
        $this->alamat = $post["alamat"];
        $this->harga = preg_replace("/[^0-9]/", "",$post["harga"]);
        $this->listrik = $post["listrik"];
        $this->angsuran = $post["tipe_angsuran"];
        $this->status = $post["status"];
        $this->oleh = $this->session->userdata('user_logged')->user_id;
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->luas_tanah = $post["luas_tanah"];
        $this->nama_penghuni = $post["nama_penghuni"];
        $this->provinsi = $post["provinsi"];
        $this->kota = $post["kotamadya"];
        $this->kecamatan = $post["kecamatan"];
        $this->alamat = $post["alamat"];
        $this->harga = preg_replace("/[^0-9]/", "",$post["harga"]);
        $this->angsuran = $post["tipe_angsuran"];
        $this->listrik = $post["listrik"];
        $this->status = $post["status"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }


}
