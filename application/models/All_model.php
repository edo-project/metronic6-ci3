<?php defined('BASEPATH') OR exit('No direct script access allowed');


class All_model extends CI_Model
{
    private $_table = "m_menu";

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getSubMenu($id){

        return $this->db->get_where($this->_table, ["is_main_menu" => $id])->result();

    }


}