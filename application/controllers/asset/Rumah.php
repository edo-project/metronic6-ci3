<?php

class Rumah extends CI_Controller
{


    private $_table_provinsi = "m_ipropinsi";
    private $_table_kota = "m_ikabkota";
    private $_table_kecamatan = "m_ikecamatan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("rumah_model");
        $this->load->library('form_validation');
        $this->load->library('curl');
        $this->load->helper('form');
        $this->load->helper('url');
    }

    public function index()
    {   
       
        $data["rumah"] = $this->rumah_model->getAll();
        $this->load->view("asset/rumah/list", $data);
    }

    public function tambah()
    {   
        

        $rumah = $this->rumah_model;
        $data['provinsi'] = $this->db->get($this->_table_provinsi)->result();
        $data['tipe_angsuran'] = $this->db->get('m_tipeangsuran')->result();
        $data['status'] = $this->db->get('m_status')->result();

        $validation = $this->form_validation;
        $validation->set_rules($rumah->rules());

        if ($validation->run()) {
            $rumah->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("asset/rumah/add", $data);
    }

    public function get_kotamadya($id=null){

        $id_post = $this->input->post('id');

        if($id_post !=null ){

            $data = $this->db->get_where($this->_table_kota, array('id_propinsi' => $id_post))->result();
            echo json_encode($data);

        }
        else{
            $data = $this->db->get_where($this->_table_kota, array('id_propinsi' => $id))->result();
            return $data;
        }
        
    }

    public function get_kecamatan($id=null){



        $id_post = $this->input->post('id');

        if($id_post !=null ){

            $data = $this->db->get_where($this->_table_kecamatan, array('id_kabkota' => $id_post))->result();
            echo json_encode($data);

        }
        else{
            $data = $this->db->get_where($this->_table_kecamatan, array('id_kabkota' => $id))->result();
            return $data;
        }
        
  
    }

    public function edit($id = null)
    {
        
        if (!isset($id)) redirect('asset/rumah');
       
        $rumah = $this->rumah_model;
        $validation = $this->form_validation;
        $validation->set_rules($rumah->rules());

        if ($validation->run()) {
            $rumah->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect('asset/rumah');
        }

        $data["rumah"] = $rumah->getById($id);
        $data['provinsi'] = $this->db->get($this->_table_provinsi)->result();
        $data['kota'] = $this->get_kotamadya($data["rumah"]->provinsi);
        $data['kecamatan'] = $this->get_kecamatan($data["rumah"]->kota);
        $data['tipe_angsuran'] = $this->db->get('m_tipeangsuran')->result();
        $data['status'] = $this->db->get('m_status')->result();


        if (!$data["rumah"]) show_404();
        
        $this->load->view("asset/rumah/edit", $data);
    }


    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->rumah_model->delete($id)) {
            redirect(site_url('asset/rumah'));
        }
    }
}
