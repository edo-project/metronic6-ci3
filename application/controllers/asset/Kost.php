<?php

class Kost extends CI_Controller
{

    private $_table_provinsi = "m_ipropinsi";
    private $_table_kota = "m_ikabkota";
    private $_table_kecamatan = "m_ikecamatan";

    public function __construct()
    {
        parent::__construct();
        $this->load->model("kost_model");
        $this->load->library('form_validation');
    }

    public function index()
    {   
        $data['list'] = $this->kost_model->getAll();
        $this->load->view("asset/Kost/list",$data);
    }

    public function tambah()
    { 
        $data = [
            
            "fasilitas" => $this->kost_model->get_fasiltas(),
            "provinsi" => $this->db->get($this->_table_provinsi)->result(),
            "status" => $this->db->get('m_status')->result(),
            "tipe_angsuran" => $this->db->get('m_tipeangsuran')->result()

            
        ];

        $kost = $this->kost_model;
        $validation = $this->form_validation;
        $validation->set_rules($kost->rules());

        if ($validation->run()) {
            $kost->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("asset/kost/add", $data);

    }

    public function get_kotamadya($id=null){

        $id_post = $this->input->post('id');

        if($id_post !=null ){

            $data = $this->db->get_where($this->_table_kota, array('id_propinsi' => $id_post))->result();
            echo json_encode($data);

        }
        else{
            $data = $this->db->get_where($this->_table_kota, array('id_propinsi' => $id))->result();
            return $data;
        }
        
    }

    public function get_kecamatan($id=null){



        $id_post = $this->input->post('id');

        if($id_post !=null ){

            $data = $this->db->get_where($this->_table_kecamatan, array('id_kabkota' => $id_post))->result();
            echo json_encode($data);

        }
        else{
            $data = $this->db->get_where($this->_table_kecamatan, array('id_kabkota' => $id))->result();
            return $data;
        }
        
  
    }

    public function edit($id = null)
    {
        
        if (!isset($id)) redirect('asset/kost');
       
        $kost = $this->kost_model;
        $validation = $this->form_validation;
        $validation->set_rules($kost->rules());

        if ($validation->run()) {
            $kost->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
            redirect('asset/kost');
        }

        $data["fasilitas"] = $this->kost_model->get_fasiltas();
        $data["kost"] = $kost->getById($id);
        $data['provinsi'] = $this->db->get($this->_table_provinsi)->result();
        $data['kota'] = $this->get_kotamadya($data["kost"]->provinsi);
        $data['kecamatan'] = $this->get_kecamatan($data["kost"]->kota);
        $data["status"] = $this->db->get('m_status')->result();
        $data["tipe_angsuran"] = $this->db->get('m_tipeangsuran')->result();

        if (!$data["kost"]) show_404();
        
        $this->load->view("asset/kost/edit", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->kost_model->delete($id)) {
            redirect(site_url('asset/kost'));
        }
    }

}
