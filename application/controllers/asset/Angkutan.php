<?php

class Angkutan extends CI_Controller
{   

    private $_table_mobil = 'm_mobil';

    public function __construct()
    {
        parent::__construct();
        $this->load->model("angkutan_model");
        $this->load->library('form_validation');
    }

    public function index()
    {   
        $data['all'] = $this->angkutan_model->getAll();
        // print_r($data['all']);die();
        $this->load->view("asset/angkutan/list",$data);
    }

    public function tambah()
    { 
        $data = [
            
            "mobil" => $this->db->get($this->_table_mobil)->result()
            
        ];

        $angkutan = $this->angkutan_model;
        $validation = $this->form_validation;
        $validation->set_rules($angkutan->rules());

        if ($validation->run()) {
            $angkutan->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("asset/angkutan/add", $data);

    }

    public function edit($id = null)
    {   
        if (!isset($id)) redirect('asset/angkutan');
       
        $angkutan = $this->angkutan_model;
        $validation = $this->form_validation;
        $validation->set_rules($angkutan->rules());

        if ($validation->run()) {
            $angkutan->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["angkutan"] = $angkutan->getById($id);
        $data['mobil'] = $this->db->get($this->_table_mobil)->result();
        if (!$data["angkutan"]) show_404();
        
        $this->load->view("asset/angkutan/edit", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->angkutan_model->delete($id)) {
            redirect(site_url('asset/angkutan'));
        }
    }
}
