<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Login Admin</title>
        <link href="<?php echo base_url('css/styles.css') ?>" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-dark">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header">
                                    <!-- <h3 class="text-center font-weight-light my-3">Login</h3> -->
                                    <center><img  src="<?php echo base_url('image/juragan.png') ?>" width="300" height="200" ></center>
                                    </div>
                                    <div class="card-body">
                                    <form action="<?= site_url('login') ?>" method="POST">
                                      <div class="form-group">
                                      <label for="email">Username / Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="username" required />
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" class="form-control" name="password" placeholder="password" required />
                                        </div>
                                        <div class="form-group">
                                            <div class="d-flex justify-content-between">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" name="rememberme" id="rememberme" />
                                                    <label class="custom-control-label" for="rememberme"> Ingat Saya</label>
                                                </div>
                                                <a href="<?= site_url('reset_password') ?>">Lupa Password?</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn btn-success w-100" value="Login" />
                                        </div>
                                           </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="register.html">butuh akun ? Silahkan register!</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="<?php echo base_url('js/scripts.js') ?>"></script>
    </body>
</html>
