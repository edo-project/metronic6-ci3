<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('asset/rumah/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="" method="post" enctype="multipart/form-data" >

                        <input type="hidden" name="id" value="<?php echo $rumah->id?>" />

						

							<div class="form-group">
								<label for="name">Luas Tanah*</label>
								<input class="form-control <?php echo form_error('name') ? 'is-invalid':'' ?>"
								 type="text" name="luas_tanah" placeholder="1x1" value="<?php echo $rumah->luas_tanah ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('name') ?>
								</div>
							</div>



							<div class="form-group">
								<label for="provinsi">Provinsi*</label>
								<select class="form-control <?php echo form_error('provinsi') ? 'is-invalid':'' ?>"
								  name="provinsi" id="provinsi" />
								  <option> </option>
								  <?php foreach ($provinsi as $p): ?>
								  	<option <?php if($p->id == $rumah->provinsi) {echo 'selected="Selected"';} ?>value="<?php echo $p->id ?>"> <?php echo $p->nama ?>  </option>
								  <?php endforeach; ?>
								</select>
								<div class="invalid-feedback">
									<?php echo form_error('provinsi') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="kotamadya">Kotamadya*</label>
								<select class="form-control <?php echo form_error('kotamadya') ? 'is-invalid':'' ?>"
								  name="kotamadya" id="kotamadya" />
								  <?php foreach ($kota as $k): ?>
								  	<option <?php if($k->id == $rumah->kota) {echo 'selected="Selected"';} ?>value="<?php echo $k->id ?>"> <?php echo $k->nama ?>  </option>
								  <?php endforeach; ?>
								</select>
								<div class="invalid-feedback">
									<?php echo form_error('kotamadya') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="kecamatan">Kecamatan*</label>
								<select class="form-control <?php echo form_error('kecamatan') ? 'is-invalid':'' ?>"
								  name="kecamatan" id="kecamatan" />
                                  <?php foreach ($kecamatan as $kc): ?>
								  	<option <?php if($kc->id == $rumah->kecamatan) {echo 'selected="Selected"';} ?>value="<?php echo $kc->id ?>"> <?php echo $kc->nama ?>  </option>
								  <?php endforeach; ?>
								</select>
								<div class="invalid-feedback">
									<?php echo form_error('kecamatan') ?>
								</div>
							</div>	

							<div class="form-group">
								<label for="alamat">Alamat Rumah*</label>
								<textarea class="form-control <?php echo form_error('alamat') ? 'is-invalid':'' ?>"
								 name="alamat" placeholder="Jalan ..."><?php echo $rumah->alamat ?></textarea>
								<div class="invalid-feedback">
									<?php echo form_error('alamat') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="harga">Harga Sewa</label>
								<input class="form-control <?php echo form_error('harga') ? 'is-invalid':'' ?>"
								 type="text" name="harga" id="harga" min="0" placeholder="" value="" />
								 
								<div class="invalid-feedback">
									<?php echo form_error('harga') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="tipe_angsuran">Tipe Angsuran*</label>
								<select class="form-control <?php echo form_error('tipe_angsuran') ? 'is-invalid':'' ?>"
								  name="tipe_angsuran" id="tipe_angsuran" />
								  <option> </option>
								  <?php foreach ($tipe_angsuran as $p): ?>
								  	<option <?php if($p->id == $rumah->angsuran) {echo 'selected="Selected"';} ?> value="<?php echo $p->id ?>"> <?php echo $p->nama ?>  </option>
								  <?php endforeach; ?>
								</select>
								<div class="invalid-feedback">
									<?php echo form_error('tipe_angsuran') ?>
								</div>
							</div>

							<div class="form-group">
								<label for="listrik">Tegangan Listik</label>
								<input class="form-control"
								 type="text" name="listrik" id="listrik" placeholder="VA" value="<?php echo $rumah->listrik ?>" />
							</div>

							<div class="form-group">
								<label for="status">Status Rumah*</label>
								<select class="form-control <?php echo form_error('status') ? 'is-invalid':'' ?>"
								  name="status" id="status" />
								  <?php foreach ($status as $p): ?>
								  	<option <?php if($p->id == $rumah->status) {echo 'selected="Selected"';} ?> value="<?php echo $p->id ?>"> <?php echo $p->status ?>  </option>
								  <?php endforeach; ?>
								</select>
								<div class="invalid-feedback">
									<?php echo form_error('status') ?>
								</div>
							</div>

							<div class="form-group nama_penghuni ">
								<label for="nama_penghuni">Nama Penghuni</label>
								<input class="form-control"
								 type="text" id="nama_penghuni" name="nama_penghuni" value="<?php echo $rumah->nama_penghuni ?>" />
						   </div>
	


							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* Wajib Diisi
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
<script>
$("#harga").val(formatRupiah("<?php echo $rumah->harga ?>", "Rp. "))
if($("#status").val() == 2){$("#nama_penghuni").val('');$(".nama_penghuni").hide();}else $(".nama_penghuni").show();


$("#provinsi").on('change',function(){

	$.ajax({
        url: "<?php echo site_url('asset/rumah/get_kotamadya') ?>",
        type: "post",
		dataType: "json",
        data: {'id' : $("#provinsi").val()} ,
        success: function (data) {
			$('#kotamadya').html('<option value=""></option>');
			var i = 0;
    		$.each(data, function(){
        		$( "#kotamadya" ).append($('<option></option>').val(data[i].id).html(data[i].nama)
        );
        i++;
    }); 
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });

})

$("#kotamadya").on('change',function(){

$.ajax({
	url: "<?php echo site_url('asset/rumah/get_kecamatan') ?>",
	type: "post",
	dataType: "json",
	data: {'id' : $("#kotamadya").val()} ,
	success: function (data) {
		$('#kecamatan').html('<option value=""></option>');
		var i = 0;

		$.each(data, function(){
			// $( "#kecamatan" ).append($('<option></option>').val(data[i].id).html(data[i].nama)
			$( "#kecamatan" ).append($('<option></option>').val(data[i].id).html(data[i].nama)
	);
	i++;
}); 
		
	},
	error: function(jqXHR, textStatus, errorThrown) {
	   console.log(textStatus, errorThrown);
	}
});

})


$("#harga").on('keyup',function(){

$("#harga").val(formatRupiah(this.value,"Rp. "))
})


function formatRupiah(angka, prefix){
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
 
	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

$("#status").on("change",function(){

if((this.value) == "2"){
	$("#nama_penghuni").val('');
	$(".nama_penghuni").hide();
}
else $(".nama_penghuni").show();
})

</script>