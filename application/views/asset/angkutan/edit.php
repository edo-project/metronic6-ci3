<!DOCTYPE html>
<html lang="en">

<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>

<body id="page-top">

	<?php $this->load->view("admin/_partials/navbar.php") ?>
	<div id="wrapper">

		<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div id="content-wrapper">

			<div class="container-fluid">

				<?php $this->load->view("admin/_partials/breadcrumb.php") ?>

				<?php if ($this->session->flashdata('success')): ?>
				<div class="alert alert-success" role="alert">
					<?php echo $this->session->flashdata('success'); ?>
				</div>
				<?php endif; ?>

				<div class="card mb-3">
					<div class="card-header">
						<a href="<?php echo site_url('asset/angkutan/') ?>"><i class="fas fa-arrow-left"></i> Back</a>
					</div>
					<div class="card-body">

						<form action="" method="post" enctype="multipart/form-data" >

                        <input type="hidden" name="id" value="<?php echo $angkutan->id?>" />

                        <div class="form-group">
								<label for="merek_mobil">Merek Mobil*</label>
								<select class="form-control <?php echo form_error('merek_mobil') ? 'is-invalid':'' ?>"
								  name="merek_mobil" id="merek_mobil" />
								  <option> </option>
                                  <?php foreach ($mobil as $m): ?>
								  	<option <?php if($m->mobil_id == $angkutan->merek_mobil) {echo 'selected="Selected"';} ?> value="<?php echo $m->mobil_id ?>"> <?php echo $m->mobil_nama ?>  </option>
								  <?php endforeach; ?>
								</select>
								<div class="invalid-feedback">
									<?php echo form_error('provinsi') ?>
								</div>
						</div>
                            
                            
                            
                            <div class="form-group">
								<label for="harga_mobil">Harga Beli Mobil*</label>
								<input class="form-control <?php echo form_error('harga_mobil') ? 'is-invalid':'' ?>"
								 type="number" name="harga_mobil" value="<?php echo $angkutan->harga_mobil ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('harga_mobil') ?>
								</div>
							</div>

                            <div class="form-group">
								<label for="plat_nomor">Plat Nomor*</label>
								<input class="form-control <?php echo form_error('plat_nomor') ? 'is-invalid':'' ?>"
								 type="text" name="plat_nomor" value="<?php echo $angkutan->plat_nomor ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('plat_nomor') ?>
								</div>
							</div>

                            <div class="form-group">
								<label for="harga">Harga Sewa</label>
								<input class="form-control <?php echo form_error('harga') ? 'is-invalid':'' ?>"
								 type="number" name="harga" value="<?php echo $angkutan->harga ?>"/>
								<div class="invalid-feedback">
									<?php echo form_error('harga') ?>
								</div>
							</div>



							<input class="btn btn-success" type="submit" name="btn" value="Save" />
						</form>

					</div>

					<div class="card-footer small text-muted">
						* Wajib Diisi
					</div>


				</div>
				<!-- /.container-fluid -->

				<!-- Sticky Footer -->
				<?php $this->load->view("admin/_partials/footer.php") ?>

			</div>
			<!-- /.content-wrapper -->

		</div>
		<!-- /#wrapper -->


		<?php $this->load->view("admin/_partials/scrolltop.php") ?>

		<?php $this->load->view("admin/_partials/js.php") ?>

</body>

</html>
<script>

$("#provinsi").on('change',function(){

	$.ajax({
        url: "<?php echo site_url('asset/rumah/get_kotamadya') ?>",
        type: "post",
		dataType: "json",
        data: {'id' : $("#provinsi").val()} ,
        success: function (data) {
			$('#kotamadya').html('<option value=""></option>');
			var i = 0;
    		$.each(data, function(){
        		$( "#kotamadya" ).append($('<option></option>').val(data[i].id).html(data[i].nama)
        );
        i++;
    }); 
			
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });

})

$("#kotamadya").on('change',function(){

$.ajax({
	url: "<?php echo site_url('asset/rumah/get_kecamatan') ?>",
	type: "post",
	dataType: "json",
	data: {'id' : $("#kotamadya").val()} ,
	success: function (data) {
		$('#kecamatan').html('<option value=""></option>');
		var i = 0;
		$.each(data, function(){
			$( "#kecamatan" ).append($('<option></option>').val(data[i].id).html(data[i].nama)
	);
	i++;
}); 
		
	},
	error: function(jqXHR, textStatus, errorThrown) {
	   console.log(textStatus, errorThrown);
	}
});

})

// $("#show_harga").on('keyup',function(){

// 	$("#show_harga").val(formatRupiah(this.value,"Rp. "))
// 	$("#harga").val(this.value)
// })


function formatRupiah(angka, prefix){
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}
 
	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

</script>