<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">
	<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<?php $this->load->view("admin/_partials/header_mobile.php") ?>
						<?php $this->load->view("admin/_partials/sidebar.php") ?>

		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
			<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
				<?php $this->load->view("admin/_partials/header_menu.php") ?>
				<div class="kt-header__topbar">
					<?php $this->load->view("admin/_partials/topbar.php") ?>
				</div>
				
						<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
						<?php $this->load->view("admin/_partials/head_content.php") ?>
						<?php $this->load->view("admin/_partials/content.php") ?>
						<?php $this->load->view("admin/_partials/footer.php") ?>
							
						</div>
				
			</div>
		</div>
		<?php $this->load->view("admin/_partials/panel.php") ?>
	</div>
</div>
</body>
</html>
<?php $this->load->view("admin/_partials/js.php") ?>

<?