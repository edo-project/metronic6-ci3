<html lang="en">

	<!-- begin::Head -->
	<head>

		<!--begin::Base Path (base relative path for assets of this page) -->
		<base href="../">

		<!--end::Base Path -->
		<meta charset="utf-8" />
		<title>Metronic | Dashboard</title>
		<meta name="description" content="Updates and statistics">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
		</script>

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<link href="<?php echo base_url('assets/custom/fullcalendar/fullcalendar.bundle.css') ?>" rel="stylesheet" type="text/css" />

		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->
		<link href="<?php echo base_url('/assets/global/vendors.bundle.css" rel="stylesheet" type="text/css') ?>" />
		<link href="<?php echo base_url('css/style.bundle.css" rel="stylesheet" type="text/css')?>" />

		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->
		<link href="<?php echo base_url('css/skins/header/base/light.css" rel="stylesheet" type="text/css')?>" />
		<link href="<?php echo base_url('css/skins/header/menu/light.css" rel="stylesheet" type="text/css')?>" />
		<link href="<?php echo base_url('css/skins/brand/dark.css" rel="stylesheet" type="text/css')?>" />
		<link href="<?php echo base_url('css/skins/aside/dark.css" rel="stylesheet" type="text/css')?>" />

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href=".media/logos/favicon.ico" />
	</head>